// Es como poner => $document.ready(function(){});
// Es para que tenga en cuenta el contenido del DOM
$(function () {

    // A cada boton accedemos por su id 

    $('#botonVerde').click(function () {
        // Le ponemos una clase y le quitamos otra
        $('#caja').addClass('verde').removeClass('rojo');
        // Le inyectamos un texto al div #caja
        $('#caja').html('Has hecho clic en el boton <b> VERDE </b>');
    });

    $('#botonRojo').click(function () {
        $('#caja').addClass('rojo').removeClass('verde');
        $('#caja').html('Has hecho clic en el boton <b> ROJO </b>');
    });

});