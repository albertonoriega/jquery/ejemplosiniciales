$(function () {

    $('.botonLenguajes').click(function () {

        // Iteramos por todos los span
        $('.lenguajes span').each(function (index, values) {
            // Comprobamos si el valor coincide con las opciones correctas
            // Para acceder al valor hay que usar .innerHTML
            if (values.innerHTML == 'Java' || values.innerHTML == 'C++' || values.innerHTML == 'C#') {
                // Para seleccionar solo el elemento que queremos usamos eq(index)
                $('.lenguajes span').eq(index).addClass('respuesta');
            }
        });

    });

    $('.botonProvincias').click(function () {

        $('.provincias span').each(function (index, values) {

            if (values.innerHTML == 'Asturias' || values.innerHTML == 'Murcia' || values.innerHTML == 'Almeria') {
                // Para seleccionar solo el elemento que queremos usamos eq(index)
                $('.provincias span').eq(index).addClass('respuesta');
            }
        });

    });
});