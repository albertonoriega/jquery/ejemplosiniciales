
$(function () {

    //cuando hacemos hover a la caja texto rojo
    $('.rojo').hover(function () {

        $('h1').addClass('rojo').removeClass('negro');
    });

    //cuando hacemos hover a la caja texto negro
    $('.negro').hover(function () {

        $('h1').addClass('negro').removeClass('rojo');
    });

    //cuando hacemos hover a la caja añadir negrita
    $('.negritaON').hover(function () {

        $('h1').addClass('negritaON');

    });

    //cuando hacemos hover a la caja quitar negrita
    $('.negritaOFF').hover(function () {

        $('h1').removeClass('negritaON');
    });

    //cuando hacemos hover a la caja fondo verde
    $('.fondoVerdeON').hover(function () {

        $('h1').addClass('fondoVerdeON');
    });

    //cuando hacemos hover a la caja quitar fondo verde
    $('.fondoVerdeOFF').hover(function () {

        $('h1').removeClass('fondoVerdeON');
    });


});