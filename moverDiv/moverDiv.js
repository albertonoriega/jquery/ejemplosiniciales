// PARA PODER USAR top y left el elemeento tiene que tener position:relative , absolute o fixed

$(function () {

    // Mover Izquierda
    $('.izquierda').click(function () {

        $(".cuadrado").animate({ left: '-=100px' });
    });

    // Mover Derecha
    $('.derecha').click(function () {

        $(".cuadrado").animate({ left: '+=100px' });
    });

    // Mover abajo
    $('.abajo').click(function () {

        $(".cuadrado").animate({ top: '+=100px' });
    });

    //Mover arriba
    $('.arriba').click(function () {

        $(".cuadrado").animate({ top: '-=100px' });
    });

});